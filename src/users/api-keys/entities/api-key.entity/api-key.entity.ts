import {Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn} from "typeorm";
import {User} from "../../../entities/user.entity";

@Entity()
export class ApiKey {
    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    key: string;

    @Column()
    uuid:string;

    @JoinTable()
    @ManyToMany((type) => User, (user) => user.apiKeys)
    user: User;
}

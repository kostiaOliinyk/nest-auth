import {Body, Controller, Delete, Get, Param, Patch, Post} from '@nestjs/common';
import {CoffeesService} from './coffees.service';
import {CreateCoffeeDto} from './dto/create-coffee.dto';
import {UpdateCoffeeDto} from './dto/update-coffee.dto';
import {ActiveUser} from "../iam/decorators/active-user.decorator";
import {ActiveUserData} from "../iam/interfaces/active-user-data.interface";
import {Policies} from "../iam/authorization/decorators/policies.decorators";
import {FrameworkContributorPolicy} from "../iam/authorization/policies/framework-contributor.policy";
import {Auth} from "../iam/authentication/decorators/auth.decorators";
import {AuthType} from "../iam/authentication/enums/auth-type.enum";

// @Auth(AuthType.Bearer, AuthType.ApiKey)
@Auth(AuthType.Bearer)
@Controller('coffees')
export class CoffeesController {
  constructor(private readonly coffeesService: CoffeesService) {}

  // @Roles(Role.Admin)
  // @Permission(CoffeesPermissions.CreateCoffee)
  @Policies(new FrameworkContributorPolicy())
  @Post()
  create(@Body() createCoffeeDto: CreateCoffeeDto) {
    return this.coffeesService.create(createCoffeeDto);
  }

  @Get()
  findAll(
      @ActiveUser() user: ActiveUserData
  ) {
    console.log(user);
    return this.coffeesService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.coffeesService.findOne(+id);
  }

  // @Roles(Role.Admin)
  // @Permission(CoffeesPermissions.UpdateCoffee)
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCoffeeDto: UpdateCoffeeDto) {
    return this.coffeesService.update(+id, updateCoffeeDto);
  }

  // @Roles(Role.Admin)
  // @Permission(CoffeesPermissions.DeleteCoffee)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.coffeesService.remove(+id);
  }
}

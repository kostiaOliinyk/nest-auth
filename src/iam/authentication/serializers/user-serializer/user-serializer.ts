import {PassportSerializer} from "@nestjs/passport";
import {User} from "../../../../users/entities/user.entity";
import {ActiveUserData} from "../../../interfaces/active-user-data.interface";

export class UserSerializer extends PassportSerializer {
    deserializeUser(payload: ActiveUserData, done: (err: Error, user: ActiveUserData) => void): any {
        done(null, payload);
    }

    serializeUser(user: User, done: (err: Error, user: ActiveUserData) => void): any {
        done(null, {
            sub: user.id,
            email: user.email,
            role: user.role,
            permissions: user.permissions,
        })
    }

}

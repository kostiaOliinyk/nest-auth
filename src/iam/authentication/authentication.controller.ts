import {Body, Controller, HttpCode, HttpStatus, Post, Res} from '@nestjs/common';
import {AuthenticationService} from "./authentication.service";
import {SignUpDto} from "./dto/sign-up.dto/sign-up.dto";
import {SignInDto} from "./dto/sign-in.dto/sign-in.dto";
import {AuthType} from "./enums/auth-type.enum";
import {Auth} from "./decorators/auth.decorators";
import {RefreshTokenDto} from "./dto/refresh-token.dto/refresh-token.dto";
import {ActiveUser} from "../decorators/active-user.decorator";
import {ActiveUserData} from "../interfaces/active-user-data.interface";
import {OtpAuthenticationService} from "./otp-authentication.service";
import { Response } from "express";
import { toFileStream } from 'qrcode'

@Auth(AuthType.Bearer,AuthType.None)
@Controller('authentication')
export class AuthenticationController {
    constructor(
        private readonly authService: AuthenticationService,
        private readonly otpAuthService: OtpAuthenticationService,) {
    }
    @Auth(AuthType.None)
    @Post('sign-up')
    signUp(@Body() signUpDto: SignUpDto) {
        return this.authService.signUp(signUpDto);
    }
    @Auth(AuthType.None)
    @HttpCode(HttpStatus.OK)
    @Post('sign-in')
    signIn(@Body() signInDto: SignInDto) {
        return this.authService.signIn(signInDto);
    }
    @Auth(AuthType.None)
    @HttpCode(HttpStatus.OK)
    @Post('refresh-tokens')
    refreshTokens(@Body() refreshTokenDto: RefreshTokenDto) {
        return this.authService.refreshTokens(refreshTokenDto);
    }

    @Auth(AuthType.Bearer)
    @HttpCode(HttpStatus.OK)
    @Post('2fa/generate')
    async generateQrCode(
        @ActiveUser() user: ActiveUserData,
        @Res() response: Response,
    ){
        const { secret, uri } = await this.otpAuthService.generateSecret(
            user.email,
        )
        await this.otpAuthService.enableTfaForUser(user.email, secret);
        response.type('png');
        return toFileStream(response,uri)
    }
}

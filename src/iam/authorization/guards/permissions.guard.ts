import {CanActivate, ExecutionContext, Injectable} from '@nestjs/common';
import {Observable} from 'rxjs';
import {Reflector} from "@nestjs/core";
import {PERMISSION_KEY} from "../decorators/permisson.decorator";
import {PermissionType} from "../permission.type";
import {ActiveUserData} from "../../interfaces/active-user-data.interface";
import {REQUEST_USER_KEY} from "../../iam.constants";


@Injectable()
export class PermissionsGuard implements CanActivate {
    constructor(private reflector: Reflector) {
    }

    canActivate(
        context: ExecutionContext,
    ): boolean | Promise<boolean> | Observable<boolean> {

        const contextPermissions = this.reflector.getAllAndOverride<PermissionType[]>(PERMISSION_KEY, [
            context.getHandler(),
            context.getClass(),
        ]);
        if (!contextPermissions) {
            return true
        }
        const user: ActiveUserData = context.switchToHttp().getRequest()[
            REQUEST_USER_KEY
            ];

        return contextPermissions.every((permission) => user.permissions?.includes(permission));
    }
}
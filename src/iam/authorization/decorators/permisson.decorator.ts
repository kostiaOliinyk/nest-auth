import {PermissionType} from "../permission.type";
import {SetMetadata} from "@nestjs/common";


export const PERMISSION_KEY = 'permission';
export const Permission = (...permisions: PermissionType[]) =>
    SetMetadata(PERMISSION_KEY, permisions)
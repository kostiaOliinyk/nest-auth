export enum CoffeesPermissions{
    CreateCoffee = 'create_coffee',
    UpdateCoffee = 'update_coffee',
    DeleteCoffee = 'Delete_coffee',
}